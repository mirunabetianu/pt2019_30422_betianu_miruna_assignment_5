package business;

import structure.MonitoredData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface HelpingMethods {
    List<MonitoredData> readFile();
    long countDays(List<MonitoredData> monitoredDataList);
    HashMap<String,Integer> countActivities(List<MonitoredData> monitoredDataList);
    HashMap<Integer, HashMap<String, Integer>> countActivitiesPerDay(List<MonitoredData> monitoredDataList);
    HashMap<MonitoredData,Integer> durationPerLine(List<MonitoredData> monitoredDataList);
    Map<String,Integer> getActivitiesDuration(List<MonitoredData> monitoredDataList);
    ArrayList<String> activitiesFilter(HashMap<String,Integer> map, List<MonitoredData> monitoredDataList);
}
