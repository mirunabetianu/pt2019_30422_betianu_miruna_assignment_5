package business;

import structure.MonitoredData;

import java.io.IOException;
import java.nio.file.Files;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Methods implements HelpingMethods {
    public List<MonitoredData> readFile(){
        String file = "Activities.txt";

        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            return stream.map(line -> line.split("\t\t"))
                    .map(a -> new MonitoredData(a[0], a[1], a[2]))
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countDays(List<MonitoredData> monitoredDataList)
    {
        return monitoredDataList.stream()
                .map(a -> Objects.requireNonNull(parseDate(a.getStart_time())).getDayOfMonth())
                .distinct()
                .count();
    }

    public HashMap<String,Integer> countActivities(List<MonitoredData> monitoredDataList)
    {
        HashMap<String,Integer> map;
        map = monitoredDataList.stream()
                .collect(Collectors.toMap(MonitoredData::getActivity, monitoredData -> 0, (a, b) -> b, HashMap::new));

        monitoredDataList
                .forEach(monitoredData -> map.put(monitoredData.getActivity(), map.get(monitoredData.getActivity()) + 1));

        return map;
    }

    public HashMap<Integer, HashMap<String, Integer>> countActivitiesPerDay(List<MonitoredData> monitoredDataList)
    {
        HashMap<Integer, HashMap<String, Integer>> map = new HashMap<>();
        ArrayList<MonitoredData> listOfDay = new ArrayList<>();
        int day = 1;
        for(MonitoredData monitoredData:monitoredDataList)
        {
            if(monitoredDataList.indexOf(monitoredData) != 0 && Objects.equals(splitDate(monitoredData.getEnd_time()), splitDate(monitoredDataList.get(monitoredDataList.indexOf(monitoredData) - 1).getEnd_time())))
            {
                listOfDay.add(monitoredData);
            }
            else {
                if(monitoredDataList.indexOf(monitoredData) == 0)
                {
                    listOfDay.add(monitoredData);
                }
                else {
                    map.put(day, countActivities(listOfDay));
                    listOfDay = new ArrayList<>();
                    day++;
                }
            }
        }
        return map;
    }

    public HashMap<MonitoredData,Integer> durationPerLine(List<MonitoredData> monitoredDataList)
    {
        HashMap<MonitoredData,Integer> map = new HashMap<>();
        monitoredDataList
                .forEach(monitoredData -> map.put(monitoredData, Math.toIntExact(ChronoUnit.MINUTES.between(Objects.requireNonNull(parseDateTime(monitoredData.getStart_time())), parseDateTime(monitoredData.getEnd_time())))));
        return map;
    }

    public Map<String,Integer> getActivitiesDuration(List<MonitoredData> monitoredDataList)
    {
        Map<String,Integer> mapActivitiesDuration;
        mapActivitiesDuration = monitoredDataList
                .stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingInt(a-> Math.toIntExact(ChronoUnit.MINUTES.between(Objects.requireNonNull(parseDateTime(a.getStart_time())), parseDateTime(a.getEnd_time()))))));
        return mapActivitiesDuration;
    }

    private boolean passesFilter(String activity, HashMap<MonitoredData,Integer> map)
    {
        int duration_over = 0;
        int duration_below = 0;
        for(MonitoredData monitoredData:map.keySet())
        {
            if(monitoredData.getActivity().equals(activity))
                if(map.get(monitoredData) > 5)
                    duration_over++;
                else duration_below++;
        }
        return (0.9 * (duration_below+duration_over) <= duration_below);
    }

    public ArrayList<String> activitiesFilter(HashMap<String,Integer> map,List<MonitoredData> monitoredDataList)
    {
        ArrayList<String> activities = new ArrayList<>();
        for(String string:map.keySet())
        {
            if(passesFilter(string,durationPerLine(monitoredDataList)))
                activities.add(string);
        }
        return activities;
    }

    private LocalDate parseDate(String string)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date = simpleDateFormat.parse(string);
            return Instant.ofEpochMilli(date.getTime())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private LocalDateTime parseDateTime(String string)
    {
        String replace = string.replace(' ', 'T');
        return LocalDateTime.parse(replace);
    }

    private String splitDate(String string)
    {
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = simpleDateFormat2.parse(string);
            return date.toString();
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
