package start;

import business.Methods;
import structure.MonitoredData;

import data.FileWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Start {
    public static void main(String[] args) {
         MonitoredData monitoredData = new MonitoredData(null,null,null);
         Methods methods = new Methods();
         System.out.println(methods.countDays(methods.readFile()));
         HashMap<String,Integer> map = methods.countActivities(methods.readFile());
         FileWriter fileWriter = new FileWriter();
         fileWriter.FileWriterTask2(map,"mapTask2.pdf");
         HashMap<Integer,HashMap<String,Integer>> mapDay = methods.countActivitiesPerDay(methods.readFile());
         fileWriter.FileWriterTask3(mapDay);
         Map<String,Integer> mapDuration = methods.getActivitiesDuration(methods.readFile());
         fileWriter.FileWriterTask2((HashMap<String,Integer>)mapDuration,"mapTask5.pdf");
         HashMap<MonitoredData,Integer> mapDurationLine = methods.durationPerLine(methods.readFile());
         fileWriter.FileWriterTask4(mapDurationLine);
         ArrayList<String> activitiesFilter = methods.activitiesFilter(map,methods.readFile());
         for(String string:activitiesFilter)
             System.out.println(string);
    }

}
