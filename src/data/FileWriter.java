package data;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import structure.MonitoredData;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;

public class FileWriter {

     public FileWriter() {}

    public void FileWriterTask2(HashMap<String,Integer> map,String fileName)
    {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(fileName));
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();

        try {
            for(String activity:map.keySet())
            {
                if(fileName.equals("mapTask2.pdf"))
                     document.add(new Paragraph(activity + " appears "+ map.get(activity)+ " times;"));
                else
                    document.add(new Paragraph(activity + " takes "+ map.get(activity)+ " minutes;"));
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }
    public void FileWriterTask3(HashMap<Integer,HashMap<String,Integer>> map)
    {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("mapTask3.pdf"));
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();

        try {
            for(Integer i:map.keySet())
            {
                document.add(new Paragraph("Day " + i.toString() + ": " + map.get(i)));
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }

    public void FileWriterTask4(HashMap<MonitoredData,Integer> map)
    {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("mapTask4.pdf"));
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        try {
            for(MonitoredData monitoredData:map.keySet())
            {
                document.add(new Paragraph(monitoredData.getStart_time() +"     "+ monitoredData.getEnd_time() +"       "+ monitoredData.getActivity() +"      "+ " duration:"+ map.get(monitoredData)+" minutes"));
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }
}
